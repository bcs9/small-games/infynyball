# Infynyball

A paddle ball game written in python.

## Controls

Player 1

- Up: W
- Down: S

Player 2

- Up: Up Arrow
- Down: Down Arrow

## Objective

First to get to 100 points wins!

There are 3 pucks.

All the Good Lucks!

## Can I Use This?

Absolutely!

Please leave the license in the same folder as the project.

If that bothers you, then please, don't use this program.

## How To Have Fun

Play the game with friends, there is no AI.